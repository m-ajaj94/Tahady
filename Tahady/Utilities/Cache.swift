//
//  Cache.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/1/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Cache{
    private static let defaults = UserDefaults.standard
    
    struct User{
        
        private static let userKey = "User"
        private static let userIDKey = "User/id"
        private static let userNameKey = "User/name"
        private static let userRoleKey = "User/role"
        private static let userImageURLKey = "User/imageURL"
        
        static func isSignedIn() -> Bool{
            return defaults.value(forKey: userKey) != nil
        }
        
        static func loginUser(data : JSON){
            let name = data["name"].string!
            let role = 2
            let id = data["id"].intValue
            defaults.set(id, forKey: userIDKey)
            let imageURL = data["image"].string!
            defaults.set(true, forKey: userKey)
            defaults.set(name, forKey: userNameKey)
            defaults.set(role, forKey: userRoleKey)
            defaults.set(imageURL, forKey: userImageURLKey)
        }
        
        static var userID : Int{
            if defaults.value(forKey: userIDKey) != nil{
                return defaults.value(forKey: userIDKey) as! Int
            }
            return -1
        }
        
        static var userImageURL : URL?{
            if defaults.value(forKey: userImageURLKey) != nil{
                return URL(string: defaults.value(forKey: userImageURLKey) as! String)
            }
            return nil
        }
        
        static var userRole : String{
            if defaults.value(forKey: userRoleKey) != nil{
                return String(defaults.value(forKey: userRoleKey) as! Int)
            }
            return ""
        }
        
        static var userName : String{
            if defaults.value(forKey: userNameKey) != nil{
                return defaults.value(forKey: userNameKey) as! String
            }
            return ""
        }
        
        
        struct FCM{
            
            static let fcmTokenKey = "FCMToken"
            
            static func getFCMToken() -> String?{
                return defaults.value(forKey: fcmTokenKey) as? String
            }
            
            static func setFCMToken(token : String){
                Cache.defaults.set(token, forKey: fcmTokenKey)
            }
        }
    }
}
