//
//  CustomToolBar.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/2/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class CustomToolbar: UIToolbar {

    override func awakeFromNib() {
        setBackgroundImage(UIImage(named: "BannerBackground"), forToolbarPosition: .any, barMetrics: .default)
    }
    
//    override func sizeThatFits(_ size: CGSize) -> CGSize {
//        return CGSize(width: superview!.frame.width, height: 60)
//    }

}
