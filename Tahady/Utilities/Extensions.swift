//
//  Extensions.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

extension UIViewController {
    func hidesKeyboardOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func slideView(){
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height
            }
        }
    }
    
    func setToolbar(){
        let soundButton = UIBarButtonItem(image: UIImage(named:"SoundIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(switchSound))
        let homeButton = UIBarButtonItem(image: UIImage(named:"HomeIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showHome))
        let settingsButton = UIBarButtonItem(image: UIImage(named:"SettingsIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(showSettings))
        let shareButton = UIBarButtonItem(image: UIImage(named:"ShareIcon")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(share))
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let array = [flexibleSpace, homeButton, flexibleSpace, soundButton, flexibleSpace, settingsButton, flexibleSpace, shareButton, flexibleSpace]
        setToolbarItems(array, animated: false)
    }
    
    @objc func switchSound(){
        
    }
    
    @objc func showHome(){
        navigationController!.popToRootViewController(animated: true)
    }
    
    @objc func showSettings(){
        
    }
    
    @objc func share(){
        
    }
    
    func showLoading(message : String) -> UIAlertController{
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        present(alert, animated: true, completion: nil)
        return alert
    }
    
    func showErrorAlert(message : String)
    {
        let alert = UIAlertController(title: "Error".localized(), message: message, preferredStyle: .alert)
        let okayButton = UIAlertAction(title: "Okay".localized(), style: .default, handler: nil)
        alert.addAction(okayButton)
        present(alert, animated: true, completion: nil)
    }
    
    func showRetryAlert(message : String, retryButton : UIAlertAction){
        let alert = UIAlertController(title: "Error".localized(), message: message, preferredStyle: .alert)
        alert.addAction(retryButton)
        let okayButton = UIAlertAction(title: "Okay".localized(), style: .default, handler: nil)
        alert.addAction(okayButton)
        present(alert, animated: true, completion: nil)
    }
    
    func showConnectionErrorAlert(){
        showErrorAlert(message: "ERROR_NO_INTERNET".localized())
    }
    
}

extension UINavigationBar {
    func setTransparent(){
        self.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.shadowImage = UIImage()
        self.isTranslucent = true
    }
}

extension String {
    var isNumeric: Bool {
        guard self.count > 0 else { return false }
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self.characters).isSubset(of: nums)
    }
    
    func removeSpaces() -> String{
        let temp = trimmingCharacters(in: .whitespacesAndNewlines)
        return temp
    }
}

extension UIImage{
    func to64Base() -> String{
        let imageData = UIImagePNGRepresentation(self)
        return imageData!.base64EncodedString(options: .lineLength64Characters)
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
