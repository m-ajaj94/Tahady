//
//  GraphicHelper.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/28/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

struct GraphicHelper{
    static func showOkayAlert(controller : UIViewController, message : String){
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .default, handler: nil)
        alert.addAction(action)
        controller.present(alert, animated: true, completion: nil)
    }
}
