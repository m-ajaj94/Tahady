//
//  DrawerTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/29/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class DrawerTableViewCell: UITableViewCell {
    
    static let nibName = "DrawerTableViewCell"

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
}
