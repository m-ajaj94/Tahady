//
//  DropDownTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/29/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import DropDown

class DropDownTableViewCell: UITableViewCell {
    
    static let nibName = "DropDownTableViewCell"

    @IBOutlet weak var cellButton: UIButton!
    
    var delegate : DropDownTableViewCellDelegate?
    var indexPath : IndexPath?
    var selectedIndex : Int?
    var data : [String]?
    var dropDown : DropDown = DropDown()
    var title : String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        cellButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        dropDown.anchorView = cellButton
        dropDown.dismissMode = .automatic
        dropDown.cellHeight = 80
        dropDown.direction = .any
        dropDown.bottomOffset = CGPoint(x: 0, y:80*0.85)
        dropDown.topOffset = CGPoint(x: 0, y:-80*0.85)
        dropDown.selectionAction = { index, string in
            self.selectedIndex = index
            self.cellButton.setTitle(self.title!.appending(": ").appending(string), for: .normal)
            self.delegate!.didChooseFromMenu(indexPath: self.indexPath!, index: index)
        }
    }
    
    func loadData(){
        dropDown.dataSource = data!
        if selectedIndex == -1{
            cellButton.setTitle(title, for: .normal)
        }
        else{
            cellButton.setTitle(title!.appending(": ").appending(data![selectedIndex!]), for: .normal)
        }
    }

    @IBAction func cellButtonPressed(_ sender: Any) {
        dropDown.show()
    }
    
}

protocol DropDownTableViewCellDelegate{
    func didChooseFromMenu(indexPath : IndexPath, index : Int)
}
