//
//  DateTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/30/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class DateTableViewCell: UITableViewCell {
    
    static let nibName = "DateTableViewCell"

    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellDatePicker: UIDatePicker!
    
    var indexPath : IndexPath?
    var currentDate : Date?
    var delegate : DateTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellDatePicker.timeZone = TimeZone.init(secondsFromGMT: 0)
        cellLabel.layer.cornerRadius = 5
        cellLabel.layer.masksToBounds = true
        backgroundColor = .clear
        cellDatePicker.maximumDate = Date()
    }
    
    @IBAction func datePickerDidChange(_ sender: Any) {
        currentDate = cellDatePicker.date
        delegate!.didChangeDate(indexPath: indexPath!, date: currentDate!)
    }
    
}

protocol DateTableViewCellDelegate{
    func didChangeDate(indexPath : IndexPath, date : Date)
}
