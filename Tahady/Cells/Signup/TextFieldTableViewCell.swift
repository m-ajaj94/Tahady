//
//  TextFieldTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class TextFieldTableViewCell: UITableViewCell {
    
    static let nibName = "TextFieldTableViewCell"
    
    @IBOutlet weak var textField: CustomTextField!
    
    var indexPath : IndexPath?
    var delegate : TextFieldTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    
    @IBAction func textFieldDidChangeText(_ sender: Any) {
        delegate!.didEditText(indexPath: indexPath!, string : textField.text!)
    }
    
}

protocol TextFieldTableViewCellDelegate{
    func didEditText(indexPath : IndexPath, string : String)
}
