//
//  SubmitButtonTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class SubmitButtonTableViewCell: UITableViewCell {
    
    static let nibName = "SubmitButtonTableViewCell"

    @IBOutlet weak var cellButton: UIButton!
    
    var indexPath : IndexPath?
    var delegate : SubmitButtonTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
    }
    @IBAction func cellButtonPressed(_ sender: Any) {
        delegate!.didPressSubmit(indexPath: indexPath!)
    }
    
}

protocol SubmitButtonTableViewCellDelegate{
    func didPressSubmit(indexPath : IndexPath)
}
