//
//  RegistrationRoleTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/22/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class RegistrationRoleTableViewCell: UITableViewCell {
    
    static let nibName = "RegistrationRoleTableViewCell"

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        selectionStyle = .none
    }
    
}
