//
//  CreateTeamTextFieldTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/9/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class CreateTeamTextFieldTableViewCell: UITableViewCell {
    
    static let nibName = "CreateTeamTextFieldTableViewCell"

    @IBOutlet weak var textField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
        textField.attributedPlaceholder = NSAttributedString(string: "Team Name".localized(), attributes: [NSAttributedStringKey.foregroundColor : UIColor.white])
    }
    
}

protocol CreateTeamTextFieldTableViewCellDelegate {
    func didEditTextField(indexPath : IndexPath, text : String)
}
