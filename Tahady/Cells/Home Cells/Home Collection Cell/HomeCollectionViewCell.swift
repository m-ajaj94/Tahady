//
//  HomeCollectionViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/3/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    static let nibName = "HomeCollectionViewCell"
    
    @IBOutlet weak var firstTeamLogo: UIImageView!
    @IBOutlet weak var secondTeamLogo: UIImageView!
    @IBOutlet weak var firstTeamName: UILabel!
    @IBOutlet weak var secondTeamLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    var challenge : Challenge?
    var indexPath : IndexPath?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func loadData(){
        if challenge!.firstTeam!.imageURL != nil{
            firstTeamLogo.kf.setImage(with: challenge!.firstTeam!.imageURL!)
        }
        if challenge!.secondTeam!.imageURL != nil{
            secondTeamLogo.kf.setImage(with: challenge!.secondTeam!.imageURL!)
        }
        if challenge!.firstTeam!.teamName != nil{
            firstTeamName.text = challenge!.firstTeam!.teamName!
        }
        if challenge!.secondTeam!.teamName != nil{
            secondTeamLabel.text = challenge!.secondTeam!.teamName!
        }
    }

}
