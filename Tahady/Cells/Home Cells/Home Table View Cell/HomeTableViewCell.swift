//
//  HomeTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/1/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import Kingfisher

class HomeTableViewCell: UITableViewCell {
    
    static let nibName = "HomeTableViewCell"
    
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamNameLabel: UILabel!
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var formationLabel: UILabel!
    @IBOutlet weak var challengeButton: UIButton!
    
    var team : Team?
    var indexPath : IndexPath?
    var delegate : HomeTableViewCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    func loadData(){
        if team!.imageURL != nil{
            teamLogo.kf.setImage(with: team!.imageURL!)
        }
        if team!.teamName != nil{
            teamNameLabel.text = team!.teamName!
        }
        if team!.distance != nil{
            distanceLabel.text = String(team!.distance!.rounded(toPlaces: 1))
        }
        if team!.planName != nil{
            formationLabel.text = team!.planName!
        }
    }
    
    @IBAction func challengeButtonPressed(_ sender: Any) {
        delegate!.didPressChallenge(indexPath: indexPath!)
    }
    
}

protocol HomeTableViewCellDelegate {
    func didPressChallenge(indexPath : IndexPath)
}
