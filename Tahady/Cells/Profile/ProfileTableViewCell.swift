//
//  ProfileTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/3/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    static let nibName = "ProfileTableViewCell"

    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var cellLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .clear
    }
    
    func getCellHeight() -> CGFloat{
        cellLabel.sizeToFit()
        return cellLabel.frame.height + cellLabel.alignmentRectInsets.bottom + cellLabel.alignmentRectInsets.top
    }
    
}
