//
//  ProfileCollectionTableViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/4/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class ProfileCollectionTableViewCell: UITableViewCell {
    
    static let nibName = "ProfileCollectionTableViewCell"

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionView.register(UINib(nibName: ProfileTeamsCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: ProfileTeamsCollectionViewCell.nibName)
    }
    
}

extension ProfileCollectionTableViewCell : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProfileTeamsCollectionViewCell.nibName, for: indexPath) as? ProfileTeamsCollectionViewCell{
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.height, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
}
