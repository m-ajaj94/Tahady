//
//  ProfileTeamsCollectionViewCell.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/4/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class ProfileTeamsCollectionViewCell: UICollectionViewCell {
    
    static let nibName = "ProfileTeamsCollectionViewCell"

    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
