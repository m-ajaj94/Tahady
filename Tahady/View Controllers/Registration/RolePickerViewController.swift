//
//  RolePickerViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/22/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import Localize_Swift

class RolePickerViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var selectedIndex : Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController!.navigationBar.setTransparent()
        tableView.register(UINib(nibName: RegistrationRoleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: RegistrationRoleTableViewCell.nibName)
        tableView.separatorStyle = .none
    }

}

extension RolePickerViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: RegistrationRoleTableViewCell.nibName) as? RegistrationRoleTableViewCell{
            switch indexPath.row{
            case 0:
                cell.userImage.image = UIImage(named: "PlayerIcon")
                cell.cellLabel.text = "Player".localized()
                return cell
            case 1:
                cell.userImage.image = UIImage(named: "CaptainIcon")
                cell.cellLabel.text = "Captain".localized()
                return cell
            case 2:
                cell.userImage.image = UIImage(named: "UmpireIcon")
                cell.cellLabel.text = "Umpire".localized()
                return cell
            case 3:
                cell.userImage.image = UIImage(named: "CoachIcon")
                cell.cellLabel.text = "Coach".localized()
                return cell
            case 4:
                cell.userImage.image = UIImage(named: "OwnerIcon")
                cell.cellLabel.text = "Field Owner".localized()
                return cell
            default:
                return UITableViewCell()
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath.row
        performSegue(withIdentifier: "ShowSignupSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSignupSegue"{
            if let controller = segue.destination as? SignUpViewController{
                controller.selectedRole = selectedIndex
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
}
