//
//  RegistrationLocationPickerViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/22/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Localize_Swift

class RegistrationLocationPickerViewController: UIViewController {
    
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var mapTitleLabel: UILabel!
    @IBOutlet weak var labelView: UILabel!
    
    var locationManager = CLLocationManager()
    var didFindLocation = false
    var selectedLocation : CLLocationCoordinate2D?
    var annotation : MKPointAnnotation?
    var userDictionary : [String : Any]?
    var userImage : UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelView.layer.zPosition = mapView.layer.zPosition + 1
        mapTitleLabel.text = "Your Location".localized()
        nextButton.isEnabled = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapMap(sender:)))
        mapView.isUserInteractionEnabled = true
        mapView.showsUserLocation = true
        mapView.addGestureRecognizer(tap)
        mapView.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
    }
    
    @objc func didTapMap(sender : UITapGestureRecognizer){
        if annotation != nil{
            mapView.removeAnnotation(annotation!)
        }
        nextButton.isEnabled = true
        selectedLocation = mapView.convert(sender.location(in: mapView), toCoordinateFrom: mapView)
        annotation = MKPointAnnotation()
        annotation!.coordinate = selectedLocation!
        mapView.addAnnotation(annotation!)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        userDictionary!["lon"] = selectedLocation!.longitude
        userDictionary!["lat"] = selectedLocation!.latitude
        let alert = UIAlertController(title: "Almost Done".localized(), message: "Are you sure?".localized(), preferredStyle: .alert)
        let yesAction = UIAlertAction(title: "Yes", style: .default) { (alert) in
            self.submitUserData()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
        alert.addAction(cancelAction)
        alert.addAction(yesAction)
        present(alert, animated: true, completion: nil)
    }
    
    func submitUserData(){
        let alert = showLoading(message: "Please wait...".localized())
        Registration.signUp(data: userDictionary!) { (data, error) in
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
                if error == nil{
                    if data!.status == 200{
                        if self.userImage != nil{
                            let id = data!.data["user_id"].int!
                            self.uploadUserImage(id : id)
                        }
                        else{
                            let alert = UIAlertController(title: "Thank You".localized(), message: "You Have Been Registered\nPlease Activate your account from the link you've recieved by E-mail".localized(), preferredStyle: .alert)
                            let okayAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                                self.navigationController!.dismiss(animated: true, completion: nil)
                            }
                            alert.addAction(okayAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    self.showConnectionErrorAlert()
                }
            }
        }
    }
    
    func uploadUserImage(id : Int){
        let alert = showLoading(message: "Uploading Image...".localized())
        Registration.uploadUserImage(image: userImage!, id: id) { (data, error) in
            alert.dismiss(animated: true, completion: nil)
            if error == nil{
                let alert = UIAlertController(title: "Thank You".localized(), message: "You Have Been Registered\nPlease Activate your account from the link you've recieved by E-mail".localized(), preferredStyle: .alert)
                let okayAction = UIAlertAction(title: "Ok", style: .default) { (alert) in
                    self.navigationController!.dismiss(animated: true, completion: nil)
                }
                alert.addAction(okayAction)
                self.present(alert, animated: true, completion: nil)
            }
            else{
                let alert = UIAlertController(title: "Error".localized(), message: "Failed to upload image".localized(), preferredStyle: .alert)
                let tryAction = UIAlertAction(title: "Try again", style: .default) { (alert) in
                    self.uploadUserImage(id: id)
                }
                let cancelAction = UIAlertAction(title: "Later", style: .destructive, handler: nil)
                alert.addAction(cancelAction)
                alert.addAction(tryAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }

}

extension RegistrationLocationPickerViewController : CLLocationManagerDelegate, MKMapViewDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !didFindLocation{
            didFindLocation = true
            let location = locations.first!
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1))
            mapView.setRegion(region, animated: true)
        }
    }
    
}
