//
//  RegistrationImagePickerViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/22/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class RegistrationImagePickerViewController: UIViewController {

    @IBOutlet weak var transparentView: UIView!
    @IBOutlet weak var uploadLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    var pickedImage : UIImage?
    var imagePicker = UIImagePickerController()
    var userDictionary : [String : Any]?
    var didSelectImage = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nextButton.isEnabled = false
        transparentView.clipsToBounds = true
        imageView.clipsToBounds = true
        transparentView.layer.cornerRadius = transparentView.frame.width/2
        imageView.layer.cornerRadius = imageView.frame.width/2
        uploadLabel.text = "Choose Photo"
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapImage))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print(userDictionary!)
    }
    
    @objc func didTapImage(){
        if pickedImage != nil{
            let alert = UIAlertController(title: "Change Image", message: nil, preferredStyle: .alert)
            let cameraAction = UIAlertAction(title: "From Camera".localized(), style: .default) { (alert) in
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let libraryAction = UIAlertAction(title: "From Library".localized(), style: .default) { (alert) in
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive)
            alert.addAction(cameraAction)
            alert.addAction(libraryAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
        else{
            let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .alert)
            let cameraAction = UIAlertAction(title: "From Camera".localized(), style: .default) { (alert) in
                self.imagePicker.sourceType = .camera
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let libraryAction = UIAlertAction(title: "From Library".localized(), style: .default) { (alert) in
                self.imagePicker.sourceType = .photoLibrary
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            let cancelAction = UIAlertAction(title: "Cancel".localized(), style: .destructive)
            alert.addAction(cameraAction)
            alert.addAction(libraryAction)
            alert.addAction(cancelAction)
            present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func skipButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "ShowMapSegue", sender: self)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        didSelectImage = true
        performSegue(withIdentifier: "ShowMapSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? RegistrationLocationPickerViewController{
            controller.userDictionary = userDictionary
            if didSelectImage{
                controller.userImage = pickedImage
            }
        }
    }
    
}

extension RegistrationImagePickerViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage
        imageView.image = pickedImage
        nextButton.isEnabled = true
        imageView.contentMode = .scaleAspectFit
        dismiss(animated: true, completion: nil)
    }
}
