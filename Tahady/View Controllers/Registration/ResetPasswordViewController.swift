//
//  ResetPasswordViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var userInfo : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: TextFieldTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: TextFieldTableViewCell.nibName)
        tableView.register(UINib(nibName: SubmitButtonTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: SubmitButtonTableViewCell.nibName)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        hidesKeyboardOnTap()
    }

}

extension ResetPasswordViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                cell.indexPath = indexPath
                cell.textField.placeholder = "Username or E-mail".localized()
                cell.delegate = self
                return cell
            }
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonTableViewCell.nibName) as? SubmitButtonTableViewCell{
                cell.indexPath = indexPath
                cell.delegate = self
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension ResetPasswordViewController : SubmitButtonTableViewCellDelegate{
    func didPressSubmit(indexPath: IndexPath) {
        if userInfo != nil{
            userInfo = userInfo!.removeSpaces()
        }
        if (userInfo == nil || userInfo == "" ){
            GraphicHelper.showOkayAlert(controller: self, message: "RESET_PASSWORD_NO_DATA".localized())
        }
        else{
            submitUserInfo()
        }
    }
    
    func submitUserInfo(){
        
    }
}

extension ResetPasswordViewController : TextFieldTableViewCellDelegate{
    func didEditText(indexPath: IndexPath, string : String) {
        userInfo = string
    }
}
