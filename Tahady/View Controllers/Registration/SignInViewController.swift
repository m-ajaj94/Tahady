//
//  SignInViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var username : String?
    var password : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController!.navigationBar.setTransparent()
        tableView.register(UINib(nibName: TextFieldTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: TextFieldTableViewCell.nibName)
        tableView.register(UINib(nibName: SubmitButtonTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: SubmitButtonTableViewCell.nibName)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        hidesKeyboardOnTap()
    }
    
}

extension SignInViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                cell.indexPath = indexPath
                if indexPath.row == 0{
                    cell.textField.placeholder = "Username".localized()
                }
                else{
                    cell.textField.placeholder = "Password".localized()
                }
                cell.delegate = self
                return cell
            }
        }
        else{
            if let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonTableViewCell.nibName) as? SubmitButtonTableViewCell{
                cell.indexPath = indexPath
                cell.delegate = self
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension SignInViewController : SubmitButtonTableViewCellDelegate{
    func didPressSubmit(indexPath: IndexPath) {
        if username != nil{
            username = username!.removeSpaces()
        }
        if password != nil{
            password = password!.removeSpaces()
        }
        if (username == nil || password == nil || username == "" || password == ""){
            GraphicHelper.showOkayAlert(controller: self, message: "SIGNIN_NO_DATA")
        }
        else{
            submitUserInfo()
        }
    }
    
    func submitUserInfo(){
        let alert = showLoading(message: "Please Wait...")
        let dict = ["user_name" : username!, "password" : password!, "os" : "iOS", "token" : Cache.User.FCM.getFCMToken()!]
        Registration.signIn(data: dict) { (data, error) in
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: {
                    if error != nil{
                        self.showConnectionErrorAlert()
                    }
                    else{
                        if data!.status == 200{
                            Cache.User.loginUser(data : data!.data)
                            self.performSegue(withIdentifier: "SuccessfulSignInSegue", sender: self)
                        }
                    }
                })
            }
        }
    }
}

extension SignInViewController : TextFieldTableViewCellDelegate{
    func didEditText(indexPath: IndexPath, string : String) {
        if indexPath.row == 0{
            username = string
        }
        else{
            password = string
        }
    }
}
