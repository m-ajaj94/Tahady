//
//  SignUpViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 9/25/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import Localize_Swift

class SignUpViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backgroundView: UIImageView!
    
    var selectedRole : Int?
    var userData : [Any] = []
    var imagePicker = UIImagePickerController()
    var pickedImage : UIImage?
    var dictionary : [String : Any]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        switch selectedRole!{
        case 0, 1:
            userData = ["", "", "", "", "", "", "", "", -1, Date()]
        case 2:
            userData = ["", "", "", "", "", "", "", "", "", Date()]
        case 3:
            userData = ["", "", "", "", "", "", "", "", Date()]
        case 4:
            userData = []
        default:
            break
        }
        tableView.register(UINib(nibName: TextFieldTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: TextFieldTableViewCell.nibName)
        tableView.register(UINib(nibName: SubmitButtonTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: SubmitButtonTableViewCell.nibName)
        tableView.register(UINib(nibName: DropDownTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DropDownTableViewCell.nibName)
        tableView.register(UINib(nibName: DateTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DateTableViewCell.nibName)
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        hidesKeyboardOnTap()
    }
}

extension SignUpViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch selectedRole!{
        case 0, 1:
            return 11
        case 2:
            return 11
        case 3:
            return 10
        case 4:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch selectedRole!{
        case 0, 1:
            if indexPath.row == 9{
                return 180
            }
            return 80
        case 2:
            if indexPath.row == 9{
                return 180
            }
            return 80
        case 3:
            if indexPath.row == 8{
                return 180
            }
            return 80
        case 4:
            return 0
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch selectedRole!{
        case 0, 1:
            switch indexPath.row{
            case 0,1,2,3,4,5,6,7:
                if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                    cell.indexPath = indexPath
                    cell.textField.text = userData[indexPath.row] as? String
                    cell.delegate = self
                    switch indexPath.row {
                    case 0:
                        cell.textField.placeholder = "Name".localized()
                        cell.textField.keyboardType = .namePhonePad
                        break
                    case 1:
                        cell.textField.placeholder = "E-mail".localized()
                        cell.textField.keyboardType = .emailAddress
                        break
                    case 2:
                        cell.textField.placeholder = "Username".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 3:
                        cell.textField.placeholder = "Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 4:
                        cell.textField.placeholder = "Repeat Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 5:
                        cell.textField.placeholder = "Phone Number".localized()
                        cell.textField.keyboardType = .phonePad
                        break
                    case 6:
                        cell.textField.placeholder = "Height".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    case 7:
                        cell.textField.placeholder = "Weight".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    default:
                        break
                    }
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 8:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DropDownTableViewCell.nibName) as? DropDownTableViewCell{
                    cell.indexPath = indexPath
                    cell.delegate = self
                    cell.selectedIndex = userData[indexPath.row] as? Int
                    cell.data = ["1", "2", "3", "4", "5", "6", "7", "8"]
                    cell.title = "Position".localized()
                    cell.loadData()
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 9:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DateTableViewCell.nibName) as? DateTableViewCell{
                    cell.currentDate = userData[indexPath.row] as? Date
                    cell.indexPath = indexPath
                    cell.delegate = self
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 10:
                if let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonTableViewCell.nibName) as? SubmitButtonTableViewCell{
                    cell.indexPath = indexPath
                    cell.cellButton.setTitle("Next".localized(), for: .normal)
                    cell.delegate = self
                    return cell
                }
            default:
                return UITableViewCell()
            }
        case 2:
            switch indexPath.row{
            case 0,1,2,3,4,5,6,7, 8:
                if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                    cell.indexPath = indexPath
                    cell.textField.text = userData[indexPath.row] as? String
                    cell.delegate = self
                    switch indexPath.row {
                    case 0:
                        cell.textField.placeholder = "Name".localized()
                        cell.textField.keyboardType = .namePhonePad
                        break
                    case 1:
                        cell.textField.placeholder = "E-mail".localized()
                        cell.textField.keyboardType = .emailAddress
                        break
                    case 2:
                        cell.textField.placeholder = "Username".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 3:
                        cell.textField.placeholder = "Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 4:
                        cell.textField.placeholder = "Repeat Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 5:
                        cell.textField.placeholder = "Phone Number".localized()
                        cell.textField.keyboardType = .phonePad
                        break
                    case 6:
                        cell.textField.placeholder = "Height".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    case 7:
                        cell.textField.placeholder = "Weight".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    case 8:
                        cell.textField.placeholder = "Umpire Price".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    default:
                        break
                    }
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 9:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DateTableViewCell.nibName) as? DateTableViewCell{
                    cell.currentDate = userData[indexPath.row] as? Date
                    cell.indexPath = indexPath
                    cell.delegate = self
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 10:
                if let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonTableViewCell.nibName) as? SubmitButtonTableViewCell{
                    cell.indexPath = indexPath
                    cell.delegate = self
                    return cell
                }
            default:
                return UITableViewCell()
            }
        case 3:
            switch indexPath.row{
            case 0,1,2,3,4,5,6,7:
                if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                    cell.indexPath = indexPath
                    cell.textField.text = userData[indexPath.row] as? String
                    cell.delegate = self
                    switch indexPath.row {
                    case 0:
                        cell.textField.placeholder = "Name".localized()
                        cell.textField.keyboardType = .namePhonePad
                        break
                    case 1:
                        cell.textField.placeholder = "E-mail".localized()
                        cell.textField.keyboardType = .emailAddress
                        break
                    case 2:
                        cell.textField.placeholder = "Username".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 3:
                        cell.textField.placeholder = "Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 4:
                        cell.textField.placeholder = "Repeat Password".localized()
                        cell.textField.keyboardType = .default
                        break
                    case 5:
                        cell.textField.placeholder = "Phone Number".localized()
                        cell.textField.keyboardType = .phonePad
                        break
                    case 6:
                        cell.textField.placeholder = "Height".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    case 7:
                        cell.textField.placeholder = "Weight".localized()
                        cell.textField.keyboardType = .numberPad
                        break
                    default:
                        break
                    }
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 8:
                if let cell = tableView.dequeueReusableCell(withIdentifier: DateTableViewCell.nibName) as? DateTableViewCell{
                    cell.currentDate = userData[indexPath.row] as? Date
                    cell.indexPath = indexPath
                    cell.delegate = self
                    return cell
                }
                else{
                    return UITableViewCell()
                }
            case 9:
                if let cell = tableView.dequeueReusableCell(withIdentifier: SubmitButtonTableViewCell.nibName) as? SubmitButtonTableViewCell{
                    cell.indexPath = indexPath
                    cell.delegate = self
                    return cell
                }
            default:
                return UITableViewCell()
            }
        case 4:
            return UITableViewCell()
        default:
            return UITableViewCell()
        }
        return UITableViewCell()
    }
    
}
extension SignUpViewController : SubmitButtonTableViewCellDelegate{
    func didPressSubmit(indexPath: IndexPath) {
        removeSpaces()
        if checkFieldsNotEmpty(){
            if emailCheck(){
                if checkPassword(){
                    if chekcIfNumbers(index: 5){
                        if chekcIfNumbers(index: 6){
                            if chekcIfNumbers(index: 7){
                                submitUserInfo()
                            }
                            else{
                                GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_WEIGHT_INCORRECT".localized())
                            }
                        }
                        else{
                            GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_HEIGHT_INCORRECT".localized())
                        }
                    }
                    else{
                        GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_PHONE_INCORRECT".localized())
                    }
                }
                else{
                    GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_PASSWORD_NOt_MATCH".localized())
                }
            }
            else{
                GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_NON_VALID_EMAIL".localized())
            }
        }
        else{
            GraphicHelper.showOkayAlert(controller: self, message: "SIGN_UP_ALL_DATA_MESSAGE".localized())
        }
    }
    
    func chekcIfNumbers(index : Int) -> Bool{
        let string = userData[index] as! String
        return string.isNumeric
    }
    
    func checkPassword() -> Bool{
        let password = userData[3] as! String
        let repeatPassword = userData[3] as! String
        return password == repeatPassword
    }
    
    func checkFieldsNotEmpty() -> Bool{
        var noEmptyField = true
        for data in userData{
            if data is String{
                noEmptyField = noEmptyField && (data as! String != "")
            }
            else if data is Int{
                noEmptyField = noEmptyField && (data as! Int != -1)
            }
        }
        return noEmptyField
    }
    
    func removeSpaces(){
        for i in 0..<userData.count {
            let data = userData[i]
            if data is String {
                userData[i] = (data as! String).removeSpaces()
            }
        }
    }
    
    func emailCheck() -> Bool{
        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        return emailPredicate.evaluate(with: userData[1] as! String)
    }
    
    func submitUserInfo(){
        switch selectedRole!{
        case 0,1:
            var dict : [String : Any] = [:]
            dict["name"] = (userData[0] as? String)!
            dict["email"] = (userData[1] as? String)!
            dict["user_name"] = (userData[2] as? String)!
            dict["password"] = (userData[3] as? String)!
            dict["mobile"] = (userData[5] as? String)!
            dict["height"] = (userData[6] as? String)!
            dict["weight"] = (userData[7] as? String)!
            dict["position_play_id"] = userData[8] as? Int
            dict["birth_date"] = Int((userData[9] as? Date)!.timeIntervalSince1970)
            dict["umpire_price"] = ""
            dictionary = dict
        case 2:
            var dict : [String : Any] = [:]
            dict["name"] = (userData[0] as? String)!
            dict["email"] = (userData[1] as? String)!
            dict["user_name"] = (userData[2] as? String)!
            dict["password"] = (userData[3] as? String)!
            dict["mobile"] = (userData[5] as? String)!
            dict["height"] = (userData[6] as? String)!
            dict["weight"] = (userData[7] as? String)!
            dict["position_play_id"] = ""
            dict["birth_date"] = Int((userData[9] as? Date)!.timeIntervalSince1970)
            dict["umpire_price"] = (userData[8] as? String)!
            dictionary = dict
        case 3:
            var dict : [String : Any] = [:]
            dict["name"] = (userData[0] as? String)!
            dict["email"] = (userData[1] as? String)!
            dict["user_name"] = (userData[2] as? String)!
            dict["password"] = (userData[3] as? String)!
            dict["mobile"] = (userData[5] as? String)!
            dict["height"] = (userData[6] as? String)!
            dict["weight"] = (userData[7] as? String)!
            dict["position_play_id"] = ""
            dict["birth_date"] = Int((userData[8] as? Date)!.timeIntervalSince1970)
            dict["umpire_price"] = ""
            dictionary = dict
        case 4:
            break
        default:
            break
        }
        performSegue(withIdentifier: "ShowImagePickerSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? RegistrationImagePickerViewController{
            controller.userDictionary = dictionary
        }
    }
}

extension SignUpViewController : TextFieldTableViewCellDelegate{
    func didEditText(indexPath: IndexPath, string: String) {
        userData[indexPath.row] = string
    }
}

extension SignUpViewController : DropDownTableViewCellDelegate{
    func didChooseFromMenu(indexPath: IndexPath, index: Int) {
        userData[indexPath.row] = index
    }
}

extension SignUpViewController : DateTableViewCellDelegate{
    func didChangeDate(indexPath: IndexPath, date: Date) {
        userData[indexPath.row] = date
    }
}

