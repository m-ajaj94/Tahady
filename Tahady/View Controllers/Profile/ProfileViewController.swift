//
//  ProfileViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/3/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import Kingfisher

class ProfileViewController: UIViewController {

    @IBOutlet weak var profileImageLabel: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var redCardsNumberLabel: UILabel!
    @IBOutlet weak var redCardsLabel: UILabel!
    @IBOutlet weak var yellowCardsNumberLabel: UILabel!
    @IBOutlet weak var yellowCadsLabel: UILabel!
    @IBOutlet weak var friendsLabel: UILabel!
    @IBOutlet weak var challengesLabel: UILabel!
    @IBOutlet weak var friendsNumberLabel: UILabel!
    @IBOutlet weak var challenegesNumberLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    let imageTitles = ["BirthdayIcon", "CountryIcon", "CityIcon", "LengthIcon", "WeightIcon", "GoalsCountIcon", "PlayerPositionIcon", "TeamsIcon", "BirthdayIcon", "TeamsIcon"]
    let titles = ["Birthday".localized(), "Country".localized(), "City".localized(), "Length".localized(), "Weight".localized(), "Goals".localized(), "Position".localized(), "Number of teams".localized(), "Date Created".localized(), "Teams".localized()]
    var cellHeights : [CGFloat] = [30, 30, 30, 30, 30, 30, 30, 30, 30, 30]
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfileContainer()
        setupTableView()
        getUserInfo()
    }
    
    func getUserInfo(){
        let alert = showLoading(message: "Loading...")
        Requests.getUser(data: ["user_id" : Cache.User.userID]) { (data, error) in
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
                if error == nil{
                    if data!.status == 200{
                        self.user = User(json : data!.data)
                        self.setData()
                    }
                    else{
                        self.showConnectionErrorAlert()
                    }
                }
                else{
                    self.showConnectionErrorAlert()
                }
            }
        }
    }
    
    func setData(){
        self.tableView.reloadData()
        if user != nil{
            if user!.imageURL != nil{
                profileImageLabel.kf.setImage(with: user!.imageURL!)
            }
            if user!.name != nil{
                nameLabel.text = user!.name!
            }
            else{
                nameLabel.text = ""
            }
            if user!.email != nil{
                emailLabel.text = user!.email!
            }
            else{
                emailLabel.text = ""
            }
            if user!.mobile != nil{
                phoneLabel.text = user!.mobile!
            }
            else{
                phoneLabel.text = ""
            }
            if user!.challengesNumber != nil{
                challenegesNumberLabel.text = String(user!.challengesNumber!)
            }
            else{
                challenegesNumberLabel.text = ""
            }
            if user!.friendsNumber != nil{
                friendsNumberLabel.text = String(user!.friendsNumber!)
            }
            else{
                friendsNumberLabel.text = ""
            }
            if user!.yellowCardsNumber != nil{
                yellowCardsNumberLabel.text = String(user!.yellowCardsNumber!)
            }
            else{
                yellowCardsNumberLabel.text = ""
            }
            if user!.redCardsNumber != nil{
                redCardsNumberLabel.text = String(user!.redCardsNumber!)
            }
            else{
                redCardsNumberLabel.text = ""
            }
        }
    }
    
    func setupProfileContainer(){
        containerView.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        containerView.layer.borderWidth = 4
        containerView.layer.cornerRadius = 6
        containerView.clipsToBounds = true
        let imageView = UIImageView(image: UIImage(named : "orangeButtonBackground"))
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 64)
        imageView.center = CGPoint(x: containerView.frame.width/2 + containerView.frame.origin.x, y: containerView.frame.origin.y)
        let imageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageView.frame.width * 0.8, height: imageView.frame.height * 0.8))
        imageLabel.center = imageView.center
        imageLabel.textAlignment = .center
        imageLabel.text = "Player Info"
        imageLabel.font = UIFont.systemFont(ofSize: 14)
        imageLabel.textColor = .white
        view.addSubview(imageView)
        view.addSubview(imageLabel)
    }
    
    func setupTableView(){
        let imageView = UIImageView(image: UIImage(named : "orangeButtonBackground"))
        tableView.separatorStyle = .none
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 64)
        imageView.center = CGPoint(x: tableViewContainer.frame.width/2 + tableViewContainer.frame.origin.x, y: tableViewContainer.frame.origin.y)
        let imageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageView.frame.width * 0.8, height: imageView.frame.height * 0.8))
        imageLabel.center = imageView.center
        imageLabel.textAlignment = .center
        imageLabel.text = "Player Details"
        imageLabel.font = UIFont.systemFont(ofSize: 14)
        imageLabel.textColor = .white
        view.addSubview(imageView)
        view.addSubview(imageLabel)
        tableViewContainer.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        tableViewContainer.layer.borderWidth = 4
        tableViewContainer.layer.cornerRadius = 6
        tableViewContainer.clipsToBounds = true
        tableView.register(UINib(nibName: ProfileTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ProfileTableViewCell.nibName)
        tableView.register(UINib(nibName: ProfileCollectionTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: ProfileCollectionTableViewCell.nibName)
        tableView.separatorStyle = .none
    }

}

extension ProfileViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if user == nil{
            return 0
        }
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeights[indexPath.row] + 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != 9{
            if let cell = tableView.dequeueReusableCell(withIdentifier: ProfileTableViewCell.nibName) as? ProfileTableViewCell{
                cell.cellImage.image = UIImage(named : imageTitles[indexPath.row])
                var text = titles[indexPath.row].appending(": ")
                switch indexPath.row{
                case 0:
                    if user!.birthdate != nil{
                        text = text.appending(user!.birthdate!)
                    }
                    break
                case 1:
                    if user!.country != nil{
                        text = text.appending(user!.country!)
                    }
                    break
                case 2:
                    if user!.city != nil{
                        text = text.appending(user!.city!)
                    }
                    break
                case 3:
                    if user!.length != nil{
                        text = text.appending(String(user!.length!))
                    }
                    break
                case 4:
                    if user!.weight != nil{
                        text = text.appending(String(user!.weight!))
                    }
                    break
                case 5:
                    if user!.goalsNumber != nil{
                        text = text.appending(String(user!.goalsNumber!))
                    }
                    break
                case 6:
                    if user!.positionID != nil{
                        text = text.appending(String(user!.positionID!))
                    }
                    break
                case 7:
                    if user!.teamsJoined != nil{
                        text = text.appending(String(user!.teamsJoined!))
                    }
                    break
                case 8:
                    if user!.createdAt != nil{
                        text = text.appending(user!.createdAt!)
                    }
                    break
                default:
                    break
                }
                cell.cellLabel.text = text
                cellHeights[indexPath.row] = max(30, cell.getCellHeight())
                return cell
            }
        }
        if let cell = tableView.dequeueReusableCell(withIdentifier: ProfileCollectionTableViewCell.nibName) as? ProfileCollectionTableViewCell{
            cell.cellImage.image = UIImage(named : imageTitles[indexPath.row])
            cell.cellLabel.text = "Teams"
            return cell
        }
        return UITableViewCell()
    }
        
}
