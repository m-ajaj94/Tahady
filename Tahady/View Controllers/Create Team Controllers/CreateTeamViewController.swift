//
//  CreateTeamViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/8/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class CreateTeamViewController: UIViewController {
    
    let addPlayersSegueIdentifier = "ShowSelectPlayers"

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        hidesKeyboardOnTap()
        tableView.register(UINib(nibName: CreateTeamTextFieldTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: CreateTeamTextFieldTableViewCell.nibName)
    }

}

extension CreateTeamViewController : UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: CreateTeamTextFieldTableViewCell.nibName) as? CreateTeamTextFieldTableViewCell{
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: addPlayersSegueIdentifier, sender: self)
    }
}
