//
//  ChallengePopupViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/10/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import Presentr

class ChallengePopupViewController: UIViewController {

    
    @IBOutlet weak var captainPhoto: UIImageView!
    @IBOutlet weak var captainLabel: UILabel!
    @IBOutlet weak var teamLogo: UIImageView!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var dateButton: UIButton!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var sendButtonLabel: UILabel!
    @IBOutlet weak var cancelButtonLabel: UILabel!
    @IBOutlet weak var picker: UIDatePicker!
    @IBOutlet weak var dismissPickerButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    var containerOrigin : CGPoint?
    var isTimePicker : Bool?
    let formatter = DateFormatter()
    var team : Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.minimumDate = Date()
        picker.date = Date()
        containerOrigin = containerView.frame.origin
        self.containerView.alpha = 0
        formatter.dateFormat = "HH:mm a"
        timeButton.setTitle("Time: ".appending(formatter.string(from: picker.date)), for: .normal)
        formatter.dateFormat = "yyyy/MM/dd"
        dateButton.setTitle("Date: ".appending(formatter.string(from: picker.date)), for: .normal)
        loadData()
    }
    
    func loadData(){
        if team!.teamName != nil{
            teamName.text = team!.teamName!
        }
        if team!.imageURL != nil{
            teamLogo.kf.setImage(with: team!.imageURL!)
        }
        if team!.captainName != nil{
            captainLabel.text = team!.captainName!
        }
        //TODO Captain image
    }
    
    func showPicker(){
        UIView.animate(withDuration: 0.3) {
            self.containerView.alpha = 1
        }
    }
    
    func hidePicker(){
        UIView.animate(withDuration: 0.3) {
            self.containerView.alpha = 0
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        let alert = showLoading(message: "Please Wait...")
//        Challenge.sendChallenge(data: [:]) { (data, error) in
//            DispatchQueue.main.async {
//                alert.dismiss(animated: true, completion: nil)
//                if error != nil{
//                    self.showConnectionErrorAlert()
//                }
//                else{
//                }
//            }
//        }
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func dateButtonPressed(_ sender: Any) {
        isTimePicker = false
        picker.datePickerMode = .date
        showPicker()
    }
    @IBAction func timeButtonPressed(_ sender: Any) {
        isTimePicker = true
        picker.datePickerMode = .time
        showPicker()
    }
    @IBAction func locationButtonPressed(_ sender: Any) {
        
    }
    @IBAction func dismissPickerButtonPressed(_ sender: Any) {
        hidePicker()
    }
    @IBAction func pickerValueChanged(_ sender: Any) {
        if isTimePicker! {
            formatter.dateFormat = "HH:mm a"
            timeButton.setTitle("Time: ".appending(formatter.string(from: picker.date)), for: .normal)
        }
        else{
            formatter.dateFormat = "yyyy/MM/dd"
            dateButton.setTitle("Date: ".appending(formatter.string(from: picker.date)), for: .normal)
        }
    }
    
}
