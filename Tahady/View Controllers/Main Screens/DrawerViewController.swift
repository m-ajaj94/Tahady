//
//  DrawerViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/29/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import SideMenu

class DrawerViewController: UIViewController {
    
    static let storyboardID = "DrawerViewController"

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var roleLabel: UILabel!
    @IBOutlet weak var userImageView: UIImageView!
    
    let titles = ["Create Team".localized(), "Book Field".localized(), "Challenges".localized(), "My Teams".localized(), "Store".localized(), "Friends".localized(), "Settings".localized(), "Share".localized()]
    let imagesTitles = ["DrawerCreateTeam", "DrawerBookField", "DrawerChallenges", "DrawerMyTeams", "DrawerStore", "DrawerFriends", "DrawerSettings", "DrawerShare"]
    var homeDelegate : HomeDrawerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: DrawerTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DrawerTableViewCell.nibName)
        tableView.bounces = false
        tableView.separatorStyle = .none
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapProfileImage))
        userImageView.isUserInteractionEnabled = true
        userImageView.addGestureRecognizer(tap)
        if Cache.User.isSignedIn(){
            if Cache.User.userImageURL != nil{
                userImageView.kf.setImage(with: Cache.User.userImageURL!)
            }
            nameLabel.text = Cache.User.userName
            roleLabel.text = Cache.User.userRole
        }
    }
    
    @objc func didTapProfileImage(){
        dismiss(animated: true, completion: {
            self.homeDelegate!.shouldPerformSegue(identifier: "ShowProfile")
        })
    }

}

extension DrawerViewController : UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return titles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: DrawerTableViewCell.nibName) as? DrawerTableViewCell{
            cell.cellLabel.text = titles[indexPath.row].localized()
            cell.cellImage.image = UIImage(named : imagesTitles[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dismiss(animated: true, completion: {
            switch indexPath.row {
            case 0:
                self.homeDelegate!.shouldPerformSegue(identifier: "ShowCreateTeam")
                break
            default:
                break
            }
        })
    }
    
}


protocol HomeDrawerDelegate{
    func shouldPerformSegue(identifier : String)
}
