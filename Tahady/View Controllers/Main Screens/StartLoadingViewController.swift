//
//  StartLoadingViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/1/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class StartLoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if Cache.User.isSignedIn(){
            self.performSegue(withIdentifier: "SignedInSegue", sender: self)
        }
        else{
            self.performSegue(withIdentifier: "NotSignedInSegue", sender: self)
        }
    }

}
