//
//  LoggedStartViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/1/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit
import SideMenu
import Presentr
import SwiftyJSON

class HomeViewController: UIViewController {
    
    let createTeamSegueIdentifier = "ShowCreateTeam"
    
    @IBOutlet weak var tableViewContrainer: UIView!
    @IBOutlet weak var collectionViewContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBAction func menuButtonPressed(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    let presenter: Presentr = {
        let width = ModalSize.full
        let height = ModalSize.custom(size: Float(UIScreen.main.bounds.height * 0.95))
        let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y: 0))
        let customType = PresentationType.popup
        let customPresenter = Presentr(presentationType: customType)
        customPresenter.transitionType = TransitionType.coverVertical
        customPresenter.dismissTransitionType = .crossDissolve
        customPresenter.roundCorners = false
        customPresenter.backgroundColor = .black
        customPresenter.backgroundOpacity = 0.3
        return customPresenter
    }()
    
    var challenges : [Challenge] = []
    var teams : [Team] = []
    var selectedTeam : Team?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController!.navigationBar.setTransparent()
        hidesKeyboardOnTap()
        setupLayout()
        getHomeData()
    }
    
    func getHomeData(){
        let alert = showLoading(message: "Loading...")
        Requests.getHome { (data, error) in
            DispatchQueue.main.async {
                alert.dismiss(animated: true, completion: nil)
                if error == nil{
                    if data!.status == 200{
                        self.parseData(data: data!.data)
                    }
                    else{
                        let alert = UIAlertController(title: "Error".localized(), message: "Something went wrong".localized(), preferredStyle: .alert)
                        let tryAction = UIAlertAction(title: "Try again", style: .default) { (alert) in
                            self.getHomeData()
                        }
                        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                        alert.addAction(cancelAction)
                        alert.addAction(tryAction)
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                else{
                    let alert = UIAlertController(title: "Error".localized(), message: "Something went wrong".localized(), preferredStyle: .alert)
                    let tryAction = UIAlertAction(title: "Try again", style: .default) { (alert) in
                        self.getHomeData()
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: nil)
                    alert.addAction(cancelAction)
                    alert.addAction(tryAction)
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func parseData(data : JSON){
        if let challengesArray = data["challenges"].array{
            for challenge in challengesArray{
                challenges.append(Challenge(json: challenge))
            }
        }
        if let teamsArray = data["nearby_teams"].array{
            for team in teamsArray{
                teams.append(Team(json: team))
            }
        }
        tableView.reloadData()
        collectionView.reloadData()
    }
    
    func setupLayout(){
        setSideMenu()
        setupTableView()
        setupCollectionView()
    }
    
    func setupTableView(){
        let imageView = UIImageView(image: UIImage(named : "orangeButtonBackground"))
        tableView.separatorStyle = .none
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 64)
        imageView.center = CGPoint(x: tableViewContrainer.frame.width/2 + tableViewContrainer.frame.origin.x, y: tableViewContrainer.frame.origin.y)
        let imageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageView.frame.width * 0.8, height: imageView.frame.height * 0.8))
        imageLabel.center = imageView.center
        imageLabel.textAlignment = .center
        imageLabel.text = "Nearby Teams"
        imageLabel.font = UIFont.systemFont(ofSize: 14)
        imageLabel.textColor = .white
        view.addSubview(imageView)
        view.addSubview(imageLabel)
        tableViewContrainer.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        tableViewContrainer.layer.borderWidth = 4
        tableViewContrainer.layer.cornerRadius = 6
        tableViewContrainer.clipsToBounds = true
        tableView.register(UINib(nibName: HomeTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: HomeTableViewCell.nibName)
    }
    
    func setupCollectionView(){
        let imageView = UIImageView(image: UIImage(named : "orangeButtonBackground"))
        imageView.frame = CGRect(x: 0, y: 0, width: 150, height: 64)
        imageView.center = CGPoint(x: collectionViewContainer.frame.width/2 + collectionViewContainer.frame.origin.x, y: collectionViewContainer.frame.origin.y)
        let imageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: imageView.frame.width * 0.8, height: imageView.frame.height * 0.8))
        imageLabel.center = imageView.center
        imageLabel.textAlignment = .center
        imageLabel.text = "Upcoming Challenges"
        imageLabel.font = UIFont.systemFont(ofSize: 13)
        imageLabel.numberOfLines = 0
        imageLabel.textColor = .white
        view.addSubview(imageView)
        view.addSubview(imageLabel)
        collectionViewContainer.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        collectionViewContainer.layer.borderWidth = 4
        collectionViewContainer.layer.cornerRadius = 6
        collectionViewContainer.clipsToBounds = true
        let leftButton = UIButton(frame: CGRect(x: 0, y: 0, width: collectionViewContainer.frame.width*0.1, height: collectionViewContainer.frame.width*0.1))
        leftButton.center = CGPoint(x: collectionViewContainer.frame.origin.x, y: collectionViewContainer.frame.origin.y + collectionViewContainer.frame.height/2)
        leftButton.setImage(UIImage(named: "HomeLeftButton"), for: .normal)
        leftButton.backgroundColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        leftButton.layer.cornerRadius = leftButton.frame.width/2
        leftButton.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        leftButton.layer.borderWidth = 2
        leftButton.clipsToBounds = true
        leftButton.addTarget(self, action: #selector(didPressLeft), for: .touchUpInside)
        view.addSubview(leftButton)
        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: collectionViewContainer.frame.width*0.1, height: collectionViewContainer.frame.width*0.1))
        rightButton.center = CGPoint(x: collectionViewContainer.frame.origin.x + collectionViewContainer.frame.width, y: collectionViewContainer.frame.origin.y + collectionViewContainer.frame.height/2)
        rightButton.setImage(UIImage(named: "HomeRightButton"), for: .normal)
        rightButton.backgroundColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0)
        rightButton.layer.cornerRadius = rightButton.frame.width/2
        rightButton.layer.borderColor = UIColor(red: 255.0/255.0, green: 208.0/255.0, blue: 18.0/255.0, alpha: 1.0).cgColor
        rightButton.layer.borderWidth = 2
        rightButton.clipsToBounds = true
        rightButton.addTarget(self, action: #selector(didPressRight), for: .touchUpInside)
        view.addSubview(rightButton)
        collectionView.register(UINib(nibName: HomeCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: HomeCollectionViewCell.nibName)
    }
    
    func setSideMenu(){
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: DrawerViewController.storyboardID) as! DrawerViewController
        controller.homeDelegate = self
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: controller)
        controller.navigationController!.isNavigationBarHidden = true
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.width * 0.8
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowChallengeSegue"{
            if let controller = segue.destination as? ChallengeViewController{
                controller.secondTeam = selectedTeam
            }
        }
    }
    
    @objc func didPressLeft(){
        
    }
    
    @objc func didPressRight(){
        
    }

}

extension HomeViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return teams.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: HomeTableViewCell.nibName) as? HomeTableViewCell{
            if indexPath.row.quotientAndRemainder(dividingBy: 2).remainder == 0{
                cell.backgroundColor = .white
            }
            else{
                cell.backgroundColor = UIColor.groupTableViewBackground
            }
            cell.team = teams[indexPath.row]
            cell.indexPath = indexPath
            cell.delegate = self
            cell.loadData()
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension HomeViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return challenges.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.nibName, for: indexPath) as? HomeCollectionViewCell{
            cell.challenge = challenges[indexPath.row]
            cell.loadData()
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
}

extension HomeViewController : HomeDrawerDelegate{
    
    func shouldPerformSegue(identifier: String) {
        performSegue(withIdentifier: identifier, sender: self)
    }
    
}

extension HomeViewController : HomeTableViewCellDelegate{
    
    func didPressChallenge(indexPath: IndexPath) {
        selectedTeam = teams[indexPath.row]
        performSegue(withIdentifier: "ShowChallengeSegue", sender: self)
    }
}


