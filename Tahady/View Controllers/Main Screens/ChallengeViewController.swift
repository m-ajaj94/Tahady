//
//  ChallengeViewController.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/12/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class ChallengeViewController: UIViewController {

    @IBOutlet weak var constraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var myTeamLabel: UILabel!
    @IBOutlet weak var secondteamLabel: UILabel!
    @IBOutlet weak var myTeamLogo: UIImageView!
    @IBOutlet weak var secondTeamLogo: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var challengeButton: UIButton!
    
    var data : [Any] = [Date(), Date(), ""]
    var secondTeam : Team?
    private var _myTeam : Team?
    var myTeam : Team?{
        get{
            return _myTeam
        }
        set(_myTeam){
            self._myTeam = _myTeam
            setData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hidesKeyboardOnTap()
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.register(UINib(nibName: DateTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DateTableViewCell.nibName)
        tableView.register(UINib(nibName: TextFieldTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: TextFieldTableViewCell.nibName)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        secondTeamLogo.kf.setImage(with: secondTeam!.imageURL)
        if secondTeam!.teamName != nil{
            secondteamLabel.text = secondTeam!.teamName!
        }
    }
    
    func setData(){
        if myTeam!.teamName != nil{
            myTeamLabel.text = myTeam!.teamName!
        }
        myTeamLogo.kf.setImage(with: myTeam!.imageURL)
    }
    
    @IBAction func challengeButtonPressed(_ sender: Any) {
        
    }
    
}

extension ChallengeViewController : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: DateTableViewCell.nibName) as? DateTableViewCell{
                cell.cellLabel.text = "Callenge Date".localized()
                cell.delegate = self
                cell.cellDatePicker.datePickerMode = .date
                cell.cellDatePicker.maximumDate = Date()
                cell.indexPath = indexPath
                cell.cellDatePicker.date = data[indexPath.row] as! Date
                return cell
            }
            return UITableViewCell()
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: DateTableViewCell.nibName) as? DateTableViewCell{
                cell.cellLabel.text = "Callenge Time".localized()
                cell.cellDatePicker.datePickerMode = .time
                cell.cellDatePicker.maximumDate = nil
                cell.indexPath = indexPath
                cell.delegate = self
                cell.cellDatePicker.date = data[indexPath.row] as! Date
                return cell
            }
            return UITableViewCell()
        case 2:
            if let cell = tableView.dequeueReusableCell(withIdentifier: TextFieldTableViewCell.nibName) as? TextFieldTableViewCell{
                cell.delegate = self
                cell.textField.placeholder = "Challenge Location".localized()
                cell.textField.text = (data[indexPath.row] as! String)
                cell.indexPath = indexPath
                return cell
            }
            return UITableViewCell()
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 75
        }
        return 130
    }
}

extension ChallengeViewController : DateTableViewCellDelegate, TextFieldTableViewCellDelegate{
    
    func didEditText(indexPath: IndexPath, string: String) {
        data[indexPath.row] = string
    }
    
    func didChangeDate(indexPath: IndexPath, date: Date) {
        data[indexPath.row] = data
    }
    
}
