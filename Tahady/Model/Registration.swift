//
//  Registration.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/16/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class Registration : RequestTemplate{
    
    static func signUp(data : [String : Any], completionHandler : @escaping (ResponseStructure?, Error?) -> ()){
        post(function: .signUp, parameters: data) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(ResponseStructure(json: data!), nil)
            }
        }
    }
    
    static func signIn(data : [String : Any], completionHandler : @escaping (ResponseStructure?, Error?) -> ()){
        post(function: .signIn, parameters: data) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(ResponseStructure(json: data!), nil)
            }
        }
    }
    
    static func uploadUserImage(image : UIImage, id : Int, completionHandler : @escaping (Any?, Error?) -> ()){
        let parameters : [String : Any] = ["image" : UIImageJPEGRepresentation(image, 1.0)!.base64EncodedData(options: .lineLength64Characters), "user_id" : id]
        post(function: .editUserImage, parameters: parameters) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(data, nil)
            }
        }
    }
    
}
