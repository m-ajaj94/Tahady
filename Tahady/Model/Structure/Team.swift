//
//  Team.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/11/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Team{
    
    var id : Int?
    var lattitude : Double?
    var logtitude : Double?
    var captainID : Int?
    var captainName : String?
    var captainUsername : String?
    var captainEmail : String?
    var teamName : String?
    var size : Int?
    var planName : String?
    var rate : Double?
    var goalsNumber : Int?
    var challengesNumber : Int?
    var imageURL : URL?
    var distance : Double?
    var isActive : Bool?
    
    init (json : JSON){
        if let id = json["id"].int{
            self.id = id
        }
        if let lattitude = json["latitude"].double{
            self.lattitude = lattitude
        }
        if let logtitude = json["longitude"].double{
            self.logtitude = logtitude
        }
        if let captainID = json["user_id"].int{
            self.captainID = captainID
        }
        if let captainName = json["name"].string{
            self.captainName = captainName
        }
        if let captainEmail = json["email"].string{
            self.captainEmail = captainEmail
        }
        if let teamName = json["team_name"].string{
            self.teamName = teamName
        }
        if let size = json["size"].int{
            self.size = size
        }
        if let planName = json["plan_name"].string{
            self.planName = planName
        }
        if let rate = json["rate"].double{
            self.rate = rate
        }
        if let goalsNumber = json["goals_no"].int{
            self.goalsNumber = goalsNumber
        }
        if let challengesNumber = json["challenges_no"].int{
            self.challengesNumber = challengesNumber
        }
        if let url = json["image"].string{
            self.imageURL = URL(string: url)
        }
        else{
            if let url = json["logo"].string{
                self.imageURL = URL(string: url)
            }
        }
        if let distance = json["distance"].double{
            self.distance = distance
        }
        if let isActive = json["is_active"].int{
            self.isActive = isActive == 1
        }
    }
    
}
