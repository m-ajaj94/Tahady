//
//  User.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/12/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import SwiftyJSON

class User{
    var id : Int?
    var username : String?
    var name : String?
    var email : String?
    var mobile : String?
    var birthdate : String?
    var length : Double?
    var weight : Double?
    var role : Int?
    var umpirePrice : Int?
    var yellowCardsNumber : Int?
    var redCardsNumber : Int?
    var rate : Double?
    var challengesNumber : Int?
    var friendsNumber : Int?
    var goalsNumber : Int?
    var teamsCreated : Int?
    var teamsJoined : Int?
    var imageURL : URL?
    var isActive : Bool?
    var positionID : Int?
    var logitiude : Double?
    var lattitiude : Double?
    var country : String?
    var city : String?
    var createdAt : String?
    
    init(json : JSON){
        if let id = json["id"].int{
            self.id = id
        }
        if let username = json["user_name"].string{
            self.username = username
        }
        if let email = json["email"].string{
            self.email = email
        }
        if let mobile = json["mobile"].string{
            self.mobile = mobile
        }
        if let birthdate = json["birth_date"].string{
            self.birthdate = birthdate
        }
        if let length = json["length"].double{
            self.length = length.rounded(toPlaces: 1)
        }
        if let weight = json["weight"].double{
            self.weight = weight.rounded(toPlaces: 1)
        }
        if let role = json["role"].int{
            self.role = role
        }
        if let price = json["umpire_price"].int{
            self.umpirePrice = price
        }
        if let yellow = json["yellow_cards_no"].int{
            self.yellowCardsNumber = yellow
        }
        if let red = json["red_cards_no"].int{
            self.redCardsNumber = red
        }
        if let rate = json["rate"].double{
            self.rate = rate.rounded(toPlaces: 1)
        }
        if let challenges = json["challenges_no"].int{
            self.challengesNumber = challenges
        }
        if let friends = json["friends_no"].int{
            self.friendsNumber = friends
        }
        if let goals = json["goals_no"].int{
            self.goalsNumber = goals
        }
        if let teams = json["teams_joined"].int{
            self.teamsJoined = teams
        }
        if let teams = json["teams_created"].int{
            self.teamsCreated = teams
        }
        if let url = json["image"].string{
            self.imageURL = URL(string: url)!
        }
        if let active = json["is_active"].int{
            self.isActive = active == 1
        }
        if let position = json["position_play_id"].int{
            self.positionID = position
        }
        if let createdAt = json["created_at"].string{
            self.createdAt = createdAt
        }
        let dict = json["location"]
        if let logtitude = dict["longitude"].double{
            self.logitiude = logtitude
        }
        if let lattitiude = dict["latitude"].double{
            self.lattitiude = lattitiude
        }
        if let country = dict["country"].string{
            self.country = country
        }
        if let city = dict["city"].string{
            self.city = city
        }
    }
}
