//
//  Challenge.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/11/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import SwiftyJSON

class Challenge{
    
    var id : Int?
    var challengeDate : String?
    var playgroundID : Int?
    var locationID : Int?
    var lattitude : Double?
    var logtitude : Double?
    var distance : Double?
    var firstTeam : Team?
    var secondTeam : Team?
    var playground : Playground?
    
    init(json : JSON){
        if let id = json["challenge_id"].int{
            self.id = id
        }
        if let lattitude = json["latitude"].double{
            self.lattitude = lattitude
        }
        if let logtitude = json["longitude"].double{
            self.logtitude = logtitude
        }
        if let challengeDate = json["challenge_date"].string{
            self.challengeDate = challengeDate
        }
        if let playgroundID = json["playground_book_id"].int{
            self.playgroundID = playgroundID
        }
        if let locationID = json["location_id"].int{
            self.locationID = locationID
        }
        if let distance = json["distance"].double{
            self.distance = distance
        }
        self.firstTeam = Team(json: json["details_team_1"])
        self.secondTeam = Team(json: json["details_team_2"])
    }
    
    init(json : JSON, firstTeam : Team, seoncdTeam : Team){
        
    }
}
