//
//  ResponseStructure.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/11/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import SwiftyJSON

class ResponseStructure{
    var status : Int
    var message : String
    var success : Bool
    var data : JSON
    
    init(json : JSON){
        if let dict = json.dictionary{
            if let meta = dict["meta"]?.dictionary{
                if meta["status"] != nil{
                    status = meta["status"]!.intValue
                }
                else{
                    status = 0
                }
                if meta["message"] != nil{
                    message = meta["message"]!.string!
                }
                else {
                    message = ""
                }
                if meta["success"] != nil{
                    let temp = meta["success"]!.intValue
                    success = temp != 0
                }
                else{
                    success = false
                }
            }
            else{
                status = 0
                message = ""
                success = false
            }
            if dict["data"] != nil{
                data = dict["data"]!
            }
            else {
                data = JSON()
            }
        }
        else{
            status = 0
            message = ""
            success = false
            data = JSON()
        }
    }
}
