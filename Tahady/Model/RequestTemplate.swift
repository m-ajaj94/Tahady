//
//  RequestTemplate.swift
//  Tahady
//
//  Created by Majd Ajaj on 10/16/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class RequestTemplate{
    
    static let baseURL = "http://www.golden-code.com/tahady/api/"
    
    static func getURL(type : FunctionType) -> URL{
        return URL(string: baseURL.appending(type.rawValue))!
    }
    
    internal static func post(function : FunctionType, parameters : [String:Any], completionHandler : @escaping (JSON?, Error?)->()){
        let url = getURL(type: function)
        let headers : HTTPHeaders = ["secret":"XCVBGFDRFTIGNBMKOLPOIYTREWSFGVCSI"]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers : headers).responseJSON { (response) in
            switch response.result {
            case .success(let value):
                completionHandler(JSON(value),nil)
            case .failure(let error):
                completionHandler(nil,error)
            }
        }
    }
    
    internal static func uploadImage(image : UIImage, url : URL, completionHandler : @escaping (Bool?)->()){
        print(url)
        let data = UIImageJPEGRepresentation(image, 1)
        let newrequest = NSMutableURLRequest(url: url)
        newrequest.httpMethod = "POST"
        newrequest.setValue( "text/html", forHTTPHeaderField: "Content-Type")
        newrequest.addValue("application/json", forHTTPHeaderField: "Content-Type" )
        let session = URLSession.shared
        let uploadTask = session.uploadTask(with: newrequest as URLRequest, from: data) {
            (data : Data?, response :URLResponse?, error: Error?) in
            if (error == nil) {
                completionHandler(true)
            }
            else{
                completionHandler(nil)
            }
            
        }
        uploadTask.resume()
    }

    
}

enum FunctionType : String{
    case signUp = "register"
    case verify = "verify"
    case signIn = "login"
    case sendChallenge = "new_challenge"
    case home = "home"
    case editUserImage = "edit_user_image"
    case getUser = "user_profile"
}
