//
//  Requests.swift
//  Tahady
//
//  Created by Majd Ajaj on 11/11/17.
//  Copyright © 2017 Majd. All rights reserved.
//

import UIKit

class Requests: RequestTemplate {
    
    static func getHome(completionHandler : @escaping (ResponseStructure?, Error?) -> ()){
        let parameters = ["user_id" : Cache.User.userID]
        post(function: .home, parameters: parameters) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(ResponseStructure(json : data!), nil)
            }
        }
    }
    
    static func sendChallenge(data : [String : Any], completionHandler : @escaping (Any?, Error?) -> ()){
        post(function: .sendChallenge, parameters: data) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(data, nil)
            }
        }
    }
    
    static func getUser(data : [String : Any], completionHandler : @escaping (ResponseStructure?, Error?) -> ()){
        post(function: .getUser, parameters: data) { (data, error) in
            if error != nil{
                completionHandler(nil, error)
            }
            else{
                completionHandler(ResponseStructure(json : data!), nil)
            }
        }
    }
    
}
